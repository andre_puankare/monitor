#!/bin/bash

set -e

#URI="postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]"
URI="postgresql://timescale:123@localhost:5432/timeseries"
PROCESSES=10
CSVPATH=/tmp/data_2m.csv
TABLE=metrics

for (( i = 0; i < 5; i++ )); do
    echo "Refreshing database..."
    psql ${URI} -c "DELETE FROM metrics;"
    python monitor send-csv --path ${CSVPATH} --uri ${URI} --table ${TABLE} --template "('%s','%s','%s')" -n ${PROCESSES} --debug false
done
