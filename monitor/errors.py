class BaseMonitorError(Exception):
    """Base error"""
    def __init__(self, msg=''):
        super().__init__()
        self.msg = msg


class BaseBenchMarkError(BaseMonitorError):
    """Base benchmark exception error"""


class BenchFileReadError(BaseBenchMarkError):
    """Read file error"""


class BenchDBError(BaseBenchMarkError):
    """DB Error"""


class DBError(BaseMonitorError):
    """DB Error"""
