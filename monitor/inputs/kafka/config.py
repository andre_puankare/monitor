from kafka.consumer import KafkaConsumer

from monitor.interfaces import (
    AbstractBaseInputConfig,
)


class InputKafkaConsumerConfig(AbstractBaseInputConfig):
    REQUIRED_SETTINGS = {
        'group_id': None,
        'bootstrap_servers': None,
    }

    def __init__(self, section, *args, **kwargs):
        self.section = section
        self.settings = dict()
        self.args = args
        self.kwargs = kwargs

    def _parse_section(self):
        for key in self.REQUIRED_SETTINGS:
            print(key)

    def get_consumer(self):
        return KafkaConsumer(
            **self.settings,
        )
