\c postgres
DROP DATABASE timeseries;
DROP USER timescale;

CREATE DATABASE timeseries;


\c timeseries
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
CREATE TABLE IF NOT EXISTS  metrics (
    time TIMESTAMP NOT NULL,
    name TEXT NOT NULL,
    gauge DOUBLE PRECISION NULL
);
SELECT create_hypertable('metrics', 'time');
CREATE INDEX IF NOT EXISTS metrics_time_name_idx ON metrics (time, name);

CREATE TABLE IF NOT EXISTS  metrics_30s_summary (
    time TIMESTAMP NOT NULL,
    name TEXT NOT NULL,
    gauge_sum DOUBLE PRECISION NULL,
    gauge_avg DOUBLE PRECISION NULL,
    gauge_min DOUBLE PRECISION NULL,
    gauge_max DOUBLE PRECISION NULL,
    points_count DOUBLE PRECISION NULL
);
SELECT create_hypertable('metrics_30s_summary', 'time');
CREATE INDEX IF NOT EXISTS
    metrics_30s_summary_time_name_idx
ON metrics_30s_summary (time, name);

CREATE USER timescale WITH ENCRYPTED PASSWORD '123';
GRANT ALL PRIVILEGES ON DATABASE timeseries to timescale;
GRANT ALL PRIVILEGES ON TABLE metrics to timescale;
GRANT ALL PRIVILEGES ON TABLE metrics_30s_summary to timescale;
