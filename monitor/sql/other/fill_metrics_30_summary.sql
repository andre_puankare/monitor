with moved_rows as (
SELECT
  time_bucket('30s', time) as bucket,
  name,
  sum(gauge) as gauge_sum,
  avg(gauge) as gauge_avg,
  min(gauge) as gauge_min,
  max(gauge) as gauge_max,
  count(*)   as points_count
FROM
  metrics
GROUP BY bucket, name)
insert into metrics_30s_summary
select * from moved_rows;