SELECT time_bucket('180m', time) AS bucket,
       count(*) as points_count,
       sum(gauge)
FROM metrics
WHERE
    name LIKE 'us-east.server-%.users'
GROUP BY
    bucket
ORDER BY bucket DESC LIMIT 1;
