SELECT time_bucket('180m', time) AS bucket,
       count(*) as points_count,
       sum(gauge)
FROM metrics
GROUP BY
    bucket
ORDER BY bucket DESC LIMIT 1;