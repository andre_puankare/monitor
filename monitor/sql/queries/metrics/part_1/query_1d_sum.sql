SELECT time_bucket('1d', time) AS bucket,
       name,
       count(*) as points_count,
       sum(gauge)
FROM metrics
GROUP BY
    bucket, name
ORDER BY bucket DESC LIMIT 1;
