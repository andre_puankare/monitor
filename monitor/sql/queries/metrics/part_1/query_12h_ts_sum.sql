SELECT time_bucket('12h', time) AS bucket,
       count(*) as points_count,
       sum(gauge)
FROM metrics
WHERE
    name = 'us-east.server-1.users'
GROUP BY
    bucket
ORDER BY bucket DESC LIMIT 1;