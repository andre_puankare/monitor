import configparser

from monitor.inputs.kafka.config import InputKafkaConsumerConfig
from monitor.outputs.postgresql.config import OutputPostgresqlConfig


class Configurator:
    """Class configure application from config file."""
    INPUT_PLUGINS = {
        'inputs.kafka': {
            'config_class': InputKafkaConsumerConfig,
        },
    }
    OUTPUTS_PLUGIN = {
        'outputs.postgresql': {
            'config_class': OutputPostgresqlConfig,
        }
    }

    def __init__(self, path, *args, **kwargs):
        self.path = path
        self.config = configparser.ConfigParser()
        self.config.read(path)

    def configure(self, *args, **kwargs):
        raise NotImplementedError
