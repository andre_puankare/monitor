from abc import ABC


class AbstractBaseInputPlugin(ABC):
    pass


class AbstractBaseOutputPlugin(ABC):
    pass


class AbstractBaseInputConfig(ABC):
    pass


class AbstractBaseOutputConfig(ABC):
    pass
