import csv
import sys

from timeit import default_timer as timer

import click

from psycopg2.errors import SyntaxError

from monitor.cli.benchmark import _connect
from monitor.errors import BenchDBError
from monitor.cli import monitor
from multiprocessing import Process


def execute(uri, sql,  conn, curr):
    conn, cur = _connect(uri)
    try:
        cur.execute(sql)
    except (TypeError, SyntaxError) as err:
        click.echo("%s" % str(err))
        sys.exit(1)
    conn.commit()


def get_chunks(l, n):
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]


@monitor.command()
@click.option('--path', '-p', type=click.Path(), required=True,
              help='Path to csv file.',)
@click.option(
    '--uri', '-u', type=str, required=True,
    help='Connection string. The general form for a connection URI: %s ' % (
        'postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]',
    ),
)
@click.option(
    '--table', '-t', type=str, default="metrics",
    help='table name',
)
@click.option(
    '--template', '-m', type=str, default="('%s','%s','%s')",
    help='insert template for raw sql',
)
@click.option(
    '--delimiter', '-r', type=str, default=',',
    help='csv delimiter',
)
@click.option(
    '--debug', '-d', type=bool, default=True,
    help='debug mode',
)
@click.option(
    '--process_num', '-n', type=int, default=10,
    help='number of process',
)
def send_csv(
        path,
        uri,
        table,
        template,
        delimiter,
        debug,
        process_num,
):
    """
    Command for sending csv data.
    """

    sql_template = 'INSERT INTO %s VALUES' % table

    try:
        with open(path) as f:
            reader = csv.reader(f, delimiter=delimiter)
            next(reader)
            data = list(reader)
    except (
            FileNotFoundError,
            IsADirectoryError,
            PermissionError,
    ):
        click.echo("Cannot read file: '%s'" % path)
        sys.exit(1)

    chunks_size = len(data)//process_num
    chunks = get_chunks(data, chunks_size)

    # Info
    click.echo("Sql template: %s" % sql_template)
    click.echo("Processes: %s" % len(chunks))
    click.echo("Chunk size: %s" % chunks_size)
    click.echo("Chunks: %s" % len(chunks))

    processes = []
    conn_cur = []

    for chunk in chunks:
        try:
            conn, cur = _connect(uri)
        except BenchDBError as err:
            click.echo("Cannot connect to database %s" % err.msg)
            sys.exit(1)

        conn_cur.append((conn, cur))

        sql = sql_template
        sql += ','.join(template % tuple(row) for row in chunk)

        p = Process(target=execute, args=(uri, sql, conn, cur))
        processes.append(p)

    start = timer()
    for p in processes:
        p.start()
    for p in processes:
        if debug:
            print("process joined: %s" % p.pid)
        p.join()
    end = timer()

    for conn, curr in conn_cur:
        conn.close()
        cur.close()

    print('time sec', end - start)
