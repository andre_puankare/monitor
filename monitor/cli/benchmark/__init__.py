import psycopg2

from psycopg2 import Error as Psycopg2Error

from monitor.errors import (
    BenchFileReadError,
    BenchDBError,
)

from monitor.cli import monitor


def read_file(path):
    """Returns file text"""
    try:
        with open(path, 'r') as file:
            text = file.read()
    except (
            FileNotFoundError,
            IsADirectoryError,
            PermissionError,
    ):
        raise BenchFileReadError("Cannot read file: '%s'" % path)

    return text


def _connect(uri):
    """Connect to database"""
    try:
        conn = psycopg2.connect(uri)
        cur = conn.cursor()
    except Psycopg2Error as err:
        raise BenchDBError('Connection error: "%s"\n%s' % (uri, str(err)))

    return conn, cur


@monitor.group()
def benchmark():
    """
    Bencmarks.
    """
