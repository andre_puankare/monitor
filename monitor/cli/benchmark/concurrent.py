import sys

import click

import requests

import asyncio
import aiopg

from psycopg2 import Error as Psycopg2Error
from psycopg2 import OperationalError


from monitor.errors import (
    BaseMonitorError,
    BenchDBError,
)
from monitor.cli.benchmark import read_file
from monitor.cli.benchmark import benchmark


def handle_asyncio_exception(loop, context):
    msg = context.get("exception", context["message"])
    click.echo(msg)


async def do_query(
        uri: str,
        sql_query: str,
        measurements: int,
        timeout: int,
):
    async with aiopg.create_pool(uri, timeout=timeout) as pool:
        async with pool.acquire() as conn:
            for _ in range(measurements):
                async with conn.cursor() as cur:
                    try:
                        await cur.execute(sql_query)
                        ret = []
                        async for row in cur:
                            ret.append(row)
                    except (
                            Psycopg2Error,
                            OperationalError,
                    ) as e:
                        raise BenchDBError(e.diag.message_primary)


def get_connection_string(url):
    connection_string = requests.get(url).text
    return connection_string


@benchmark.command()
@click.option('--path', '-p', type=click.Path(), required=True,
              help='Path to sql code file.',)
@click.option(
    '--uri', '-u', type=str, required=False,
    help='Connection string. The general form for a connection URI: %s ' % (
        'postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]',
    ),
)
@click.option('--measurements', '-m', type=int, default=50,
              help='The number of database queries',)
@click.option('--pools', '-s', type=int, default=4,
              help='Pools.',)
@click.option('--balancer', '-b', type=str, required=False,
              help='url to get connection string',)
@click.option('--timeout', '-t', type=int, default=120,
              help='Connection timeout.',)
def concurrent(
        path,
        uri,
        measurements,
        pools,
        balancer,
        timeout,
        **kwargs,
):
    """
    Command for emulate concurrent queries to the database.
    """
    if uri is None and balancer is None:
        click.echo("you must provide connection string or url to get connection string")
        sys.exit(1)

    try:
        sql_query = read_file(path)
    except BaseMonitorError as e:
        click.echo(e.msg)
        sys.exit(1)

    loop = asyncio.get_event_loop()
    tasks = []
    for index in range(pools):
        tasks.append(do_query(
            uri=get_connection_string(balancer) if balancer else uri,
            sql_query=sql_query,
            measurements=measurements,
            timeout=timeout,
        ))
    loop.set_exception_handler(handle_asyncio_exception)
    loop.run_until_complete(asyncio.wait(tasks))






