from datetime import datetime, timedelta
import random

import click
from monitor.cli.benchmark import benchmark


def show_info(
        query_template,
        days,
        points_count,
        names_count,
        start_datetime,
        end_datetime,
        step,
):
    click.echo('Sql template: %s' % query_template)
    click.echo('Days interval: %s' % days)
    click.echo('Points to generate: %s' % points_count)
    click.echo('Different metric names: %s' % names_count)
    click.echo('Start date: %s' % start_datetime)
    click.echo('End date: %s' % end_datetime)
    click.echo('Step: %s seconds' % step)


def get_metric_names(count=1):
    return [
        'us-east.server-%s.users' % i
        for i in range(
            1,
            count if count >=2 else 2,
        )
    ]


def gen_test_sql(
        days,
        points_count,
        metric_names_count,
        start_timestamp,
):
    query_template = 'INSERT INTO metrics(time, name, gauge) VALUES %s;'
    query_data_template = "('%s','%s','%s')"

    metric_names = get_metric_names(metric_names_count)
    data = []

    start_datetime = datetime.utcfromtimestamp(start_timestamp)
    end_datetime = start_datetime + timedelta(days=days) - timedelta(seconds=1)

    step = (end_datetime - start_datetime).total_seconds() / points_count

    curr_datetime = start_datetime
    while curr_datetime < end_datetime:
        data.append(
            query_data_template % (
                curr_datetime.isoformat(),
                random.choice(metric_names),
                random.randint(1, 10e6),
            )
        )
        curr_datetime += timedelta(seconds=step)

    show_info(
        query_template=query_template,
        days=days,
        points_count=points_count,
        names_count=metric_names_count,
        start_datetime=start_datetime.isoformat(),
        end_datetime=end_datetime.isoformat(),
        step=step,
    )

    return query_template % ','.join(data)


@benchmark.command()
@click.option('-d', '--days',
              help='Days interval.', type=int,
              default=1, required=False)
@click.option('-c', '--points_count',
              help='Points count to generate.', type=int,
              default=10e5, required=False)
@click.option('-n', '--metric_names_count',
              help='Different metrics names count.', type=int,
              default=1001, required=False)
@click.option('-s', '--start_timestamp',
              help='Start timestamp.', type=int,
              default=1572912000, required=False)
@click.option('--output', '-o', type=click.Path(),
              default='%s.sql' % datetime.utcnow().isoformat(),
              help='Output file to write sql code.',)
def test_data(
        days,
        points_count,
        metric_names_count,
        start_timestamp,
        output,
        **kwargs,
):
    """
    Command for generating test data as sql code.
    """
    data = gen_test_sql(
        days=days,
        points_count=points_count,
        metric_names_count=metric_names_count,
        start_timestamp=start_timestamp,
    )
    try:
        with open(output, 'w') as file:
            file.write(data)
    except (
            FileNotFoundError,
            IsADirectoryError,
            PermissionError,
    ):
        click.echo("Cannot write to file: %s" % output)
        exit(1)
