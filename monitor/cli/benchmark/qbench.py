import sys

from datetime import datetime
from timeit import default_timer as timer

import click
import numpy

from psycopg2 import Error as Psycopg2Error


from monitor.errors import (
    BaseBenchMarkError,
    BenchDBError,
)
from monitor.cli.benchmark import _connect, read_file
from monitor.cli.benchmark import benchmark


def _calculate(time_samples, precision):
    return dict(
        measurements=len(time_samples),
        min_time=round(numpy.min(time_samples), precision),
        max_time=round(numpy.max(time_samples), precision),
        avg_time=round(numpy.average(time_samples), precision),
        percentiles=(
            round(numpy.percentile(time_samples, 50), precision),
            round(numpy.percentile(time_samples, 75), precision),
            round(numpy.percentile(time_samples, 95), precision),
        )
    )


def _gen_report(
            sql_query,
            measurements,
            min_time,
            max_time,
            avg_time,
            percentiles,
):
    report = [
        'SQL: "%s"' % sql_query.strip(),
        'Measurements: %s' % measurements,
        'Min time: %s seconds' % min_time,
        'Max time: %s seconds' % max_time,
        'Avg time: %s seconds' % avg_time,
        'Percentile 50/75/95: %s/%s/%s seconds' % percentiles
    ]
    return '\n'.join(report)


def test(
        cur,
        sql_query_path,
        measurements,
        precision,
):
    """Test query."""
    sql_query = read_file(sql_query_path)

    time_samples = []
    for _ in range(measurements):
        start = timer()
        try:
            cur.execute(sql_query)
            data = cur.fetchall()
        except Psycopg2Error as e:
            raise BenchDBError(e.diag.message_primary)
        end = timer()
        time_samples.append(end - start)

    return _gen_report(
        sql_query=sql_query,
        **_calculate(
            time_samples=time_samples,
            precision=precision,
        ),
    )


@benchmark.command()
@click.option('--path', '-p', type=click.Path(), required=True,
              help='Path to sql code file.',)
@click.option(
    '--uri', '-u', type=str, required=True,
    help='Connection string. The general form for a connection URI: %s ' % (
        'postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]',
    ),
)
@click.option('--measurements', '-m', type=int, default=50,
              help='The number of database queries',)
@click.option('--precision', '-r', type=int, default=4,
              help='Report numeric precision.',)
@click.option('--output', '-o', type=click.Path(),
              default='%s.txt' % datetime.utcnow().isoformat(),
              help='Output file.',)
def query(
        path,
        uri,
        measurements,
        precision,
        output,
        **kwargs,
):
    """
    Command for measuring database query execution time.
    """
    try:
        conn, cur = _connect(uri)
        query_report = test(**dict(
                cur=cur,
                sql_query_path=path,
                measurements=measurements,
                precision=precision,
            ))
        with open(output, 'w') as file:
            click.echo(query_report, file=file)
        click.echo('Output file: %s' % output)
    except BaseBenchMarkError as e:
        click.echo("status error:", e.msg)
        sys.exit(1)
    else:
        cur.close()
        conn.close()
