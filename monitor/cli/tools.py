import click

from monitor.cli import monitor


SAMPLE_CONFIGURATION = """
# Monitor Configuration
#

# Configuration for monitor sender agent
[agent]
## Monitor will send metrics to outputs in batches of at most
## metric_batch_size metrics
## This controls the size of write that Monitor sends to output plugins
metric_batch_size=10
metric_buffer_limit=10000


##########################################
#              INPUTS                    #
##########################################


# [inputs.kafka]
# topic="my-topic"
# group_id="my-group"
# bootstrap_servers=['localhost:9092']


##########################################
#              OUTPUTS                   #
##########################################

# [outputs.timescaledb]
# url="postgres://user:password@localhost/schema?sslmode=disable"
# table="metrics"
"""


@monitor.command()
@click.pass_obj
def config(*args, **kwargs):
    """
    Print out full sample configuration to stdout.
    """
    click.echo(SAMPLE_CONFIGURATION)
