import click

from traitlets.config.loader import Config
from IPython import start_ipython

from monitor.cli import monitor


def ipython(**locals_):
    start_ipython(
        argv=[],
        user_ns=locals_,
        config=Config(
            HistoryManager={
                'enabled': True,
            },
            TerminalInteractiveShell={
                'confirm_exit': False,
                'term_title': False,
            },
            TerminalIPythonApp={
                'display_banner': False,
            },
        ),
    )


@monitor.command()
@click.pass_obj
def shell(config):
    """
    Open up a Python shell with Monitor prec. in it.
    """
    ipython()
