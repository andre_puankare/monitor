import sys

from datetime import datetime
from timeit import default_timer as timer

import click
import numpy

from psycopg2 import Error as Psycopg2Error


from monitor.errors import (
    BaseBenchMarkError,
    BenchDBError,
    BenchFileReadError,
)
from monitor.cli.benchmark import _connect, read_file
from monitor.cli import monitor
from multiprocessing import Process


def execute(uri, sql, conn, curr):
    conn, cur = _connect(uri)
    cur.execute(sql)
    conn.commit()


@monitor.command()
@click.option('--path', '-p', type=click.Path(), required=True,
              help='Path to sql code file.',)
@click.option(
    '--uri', '-u', type=str, required=True,
    help='Connection string. The general form for a connection URI: %s ' % (
        'postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]',
    ),
)
@click.option(
    '--debug', '-d', type=bool, default=True,
    help='debug mode',
)
@click.option(
    '--process_num', '-n', type=int, default=10,
    help='number of process',
)
def send(
        path,
        uri,
        debug,
        process_num,
):
    """
    Command for sending data.
    """
    try:
        sql = read_file(path)
    except BenchFileReadError as e:
        click.echo(e.msg)
        sys.exit(1)

    processes = []
    conn_cur = []
    for i in range(process_num):
        conn, cur = _connect(uri)
        conn_cur.append((conn, cur))
        p = Process(target=execute, args=(uri, sql, conn, cur))
        processes.append(p)

    start = timer()
    for p in processes:
        p.start()
    for p in processes:
        if debug:
            print(p.pid)
        p.join()
    end = timer()

    for conn, curr in conn_cur:
        conn.close()
        cur.close()

    print('time sec', end - start)
