import click

from monitor.cli import monitor
from monitor.config import Configurator


@monitor.command()
@click.option(
    '--config', type=click.Path(),
    required=True, help='Configuration file to load.',
)
def run(config):
    """
    Run Monitor.
    """
    click.echo(config)
    configuration = Configurator(path=config)
