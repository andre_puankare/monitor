import importlib
import pkgutil

import click


class LazyConfig:
    def __init__(self, *args, **kwargs):
        self.__args = args
        self.__kwargs = kwargs
        self.__config = None


@click.group(context_settings={'help_option_names': ['-h', '--help']})
@click.pass_context
def monitor(ctx):
    ctx.obj = LazyConfig()


for _, name, _ in pkgutil.walk_packages(__path__, prefix=__name__ + "."):
    importlib.import_module(name)
