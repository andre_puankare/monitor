#!/bin/bash

set -e

PREPARE_DB_PATH="monitor/sql/prepare_db.sql"
# DATABASE_NAME="timeseries"

TEST_DAY_INTERVAL=1
TEST_POINTS_COUNT=100000
TEST_DIFFERENT_METRICS=1
TEST_DATA_OUTPUT_FILE="/tmp/test_data.sql"

FILL_METRICS_30S_SUMMARY_TABLE_PATH="monitor/sql/other/fill_metrics_30_summary.sql"

CONCURENT_QUERIES_CONNECTIONS=$1 # connections to postgresql
CONCURENT_QUERIES_EXECUTE_NUMBER=1000000
CONCURENT_QUERIES_TIMEOUT=1200
CONCURENT_QUERIES_SQL_PATH="monitor/sql/queries/metrics/part_1/query_1d_all_sum.sql"
ADMIN_CONNECTION_URI="postgres://postgres:123@localhost:5432/"
CONNECTION_URI="postgres://timescale:123@localhost:5432/timeseries"
URL_TO_GET_CONNECTION_STRING="http://localhost:8080"

TEST_QUERY_PART_1_PATHS="
monitor/sql/queries/metrics/part_1/query_15m_sum_without_group_by.sql
monitor/sql/queries/metrics/part_1/query_180m_sum_without_group_by.sql
monitor/sql/queries/metrics/part_1/query_1d_sum_without_group_by.sql
monitor/sql/queries/metrics/part_1/query_15m_sum.sql
monitor/sql/queries/metrics/part_1/query_180m_sum.sql
monitor/sql/queries/metrics/part_1/query_1d_sum.sql
monitor/sql/queries/metrics/part_1/query_12h_ts_sum.sql
monitor/sql/queries/metrics/part_1/query_15m_all_sum.sql
monitor/sql/queries/metrics/part_1/query_180m_all_sum.sql
monitor/sql/queries/metrics/part_1/query_1d_all_sum.sql
"
TEST_QUERY_PART_2_PATHS="
monitor/sql/queries/metrics/part_2/metrics_all_raw_datapoints.sql
monitor/sql/queries/metrics/part_2/metrics_30s_summary_preaggregated_2880_buckets.sql
monitor/sql/queries/metrics/part_2/metrics_group_by_30s_summary_real_time_aggregate_2880_buckets.sql
"

TEST_QUERY_MEASUREMENTS=4
OUTPUT_DIR="/tmp/"


if [ -z "$1" ]
  then
    CONCURENT_QUERIES_CONNECTIONS=1
  else
    CONCURENT_QUERIES_CONNECTIONS=$1
fi


echo "Generating test datapoints..."
python monitor benchmark test-data \
                --days ${TEST_DAY_INTERVAL} \
                --points_count ${TEST_POINTS_COUNT} \
                --output ${TEST_DATA_OUTPUT_FILE} \
                --metric_names_count ${TEST_DIFFERENT_METRICS}

echo "Refreshing database..."
# sudo -u postgres psql -d ${DATABASE_NAME} -f "${PREPARE_DB_PATH}"
# sudo -u postgres psql -d ${DATABASE_NAME} -f "${TEST_DATA_OUTPUT_FILE}"
# sudo -u postgres psql -d ${DATABASE_NAME} -f "${FILL_METRICS_30S_SUMMARY_TABLE_PATH}"

psql ${ADMIN_CONNECTION_URI} -f "${PREPARE_DB_PATH}"

# Ingest test datapoints
psql ${CONNECTION_URI} -f "${TEST_DATA_OUTPUT_FILE}"
psql ${CONNECTION_URI} -f "${FILL_METRICS_30S_SUMMARY_TABLE_PATH}"


echo "Starting concurent queries: $CONCURENT_QUERIES_CONNECTIONS"
python monitor benchmark concurrent \
                  --path  ${CONCURENT_QUERIES_SQL_PATH}\
                  --balancer ${URL_TO_GET_CONNECTION_STRING} \
                   -s ${CONCURENT_QUERIES_CONNECTIONS} \
                   -m ${CONCURENT_QUERIES_EXECUTE_NUMBER} \
                   -t ${CONCURENT_QUERIES_TIMEOUT} &

echo "Concurent queries pid [$!]"
CONCURENT_QUERIES_PID=$!


for query_path in $TEST_QUERY_PART_2_PATHS; do
python monitor benchmark query \
                  --path ${query_path} \
      -o ${OUTPUT_DIR}$(basename -a -s .sql $query_path)_CQ_${CONCURENT_QUERIES_CONNECTIONS}_$(echo `date --iso-8601='seconds'`).txt \
                  --uri ${CONNECTION_URI} \
                  -m ${TEST_QUERY_MEASUREMENTS}
done

echo "Killing concurent queries [$!]"
kill $CONCURENT_QUERIES_PID
echo "Done"
