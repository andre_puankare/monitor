import os
from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()


requires = [
    'wheel',
    'psycopg2-binary',
    'ipython',
    'flake8',
    'pytest',
    'click',
    'python-dotenv',
    'numpy',
    'aiopg',
    'kafka-python',
    'requests',
]

dev_requires = [
    'pylint',
    'ipdb',
]

setup(
    name='monitor',
    version='0.1',
    description='Monitoring',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Framework ::',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='Yushin Andrey',
    author_email='levtolstoy123123@gmail.com',
    url='',
    keywords='monitor',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    extras_require={
        'dev': dev_requires
    },
)
