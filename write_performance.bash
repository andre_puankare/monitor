#!/bin/bash

set -e

#URI="postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]"
URI="postgresql://timescale:123@localhost:5432/timeseries"
PROCESSES=50
SQLPATH=/tmp/data_10k.sql

for (( i = 0; i < 50; i++ )); do
    echo "Refreshing database..."
    psql ${URI} -c "DELETE FROM metrics;"
    python monitor send --path ${SQLPATH} --uri ${URI} -n ${PROCESSES} -d false
done
